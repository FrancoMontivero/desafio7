import express from 'express';

import { getVisits, Visits } from '../controllers/visits';

const routerVisits = express.Router();

routerVisits.get('/visitas', (req, res) => {
	let visits: Visits = getVisits(); 
	res.json({visitas: visits});
})

export default routerVisits;