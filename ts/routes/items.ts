import express from 'express';

const routerItems = express.Router();

import { Item } from '../controllers/items';

import { addVisit } from '../controllers/visits';
import { getItemsFromFile } from '../controllers/items';
import { getItemsFromFileRandom } from '../controllers/items';

routerItems.get('/items', (req, res) => {
    addVisit('items')
    .then(() => {
        const items: Item[] | Error = getItemsFromFile();
        if(items instanceof Error){
            console.log(items.message);
            res.send(items.message);
        }
        else res.json(items);        
    })
    .catch(err => {
        console.log(err);
    })
})

routerItems.get('/item-random', (req, res) => {
    addVisit('item')
    .then(() => {
        const item: Item | Error = getItemsFromFileRandom();
        if(item instanceof Error) {
            console.log(item.message)
            res.send(item.message)
        };
        res.json(item);        
    })
    .catch(err => {
        console.log(err);
    })
})

export default routerItems;
