import express from 'express';

const router = express.Router();

import routerItems from './items';
import routerVisits from './visits';

router.use(routerItems);
router.use(routerVisits);

export default router;
