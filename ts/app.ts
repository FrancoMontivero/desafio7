import express from 'express';

import cors from './middlewares/cors';
import router from './routes/index';
import morgan from './middlewares/morgan'

import { PORT } from './config';

const app = express();

app.use(cors);
app.use(morgan);
app.use(express.urlencoded({ extended : true }));

app.use(router);

app.set('port', PORT);

export default app;
