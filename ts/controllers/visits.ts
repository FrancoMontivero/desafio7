import fs from 'fs';
import path from 'path';

export interface Visits {
    item: number;
    items: number;
}

const PATH_FILE_VISIT: string = path.resolve(__dirname, '../', '../', 'visits.txt');

export function addVisit(param: 'item' | 'items'): Promise<Visits | Error> {
    return new Promise((resolve, reject) => {
        let visits: Visits = getVisits();
        visits[param]++;
        fs.writeFile(
            PATH_FILE_VISIT,
            JSON.stringify(visits),
            err => {
                if(err) reject(err);
                else resolve(visits);
            }
        )
    })
}

export function getVisits(): Visits {
    let response: string = fs.readFileSync(
        PATH_FILE_VISIT, 
        { encoding: 'utf-8', flag: 'a+' }
    );
    return response ? JSON.parse(response) : {item: 0, items: 0};
}