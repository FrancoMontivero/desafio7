import fs from 'fs';
import path from 'path'

export interface Item {
    id: number;
    title: string;
    price: number;
    thumbnail: string;
}

export function getItemsFromFile(): Item[] | Error {
    try {
        let response: string = fs.readFileSync(
            path.resolve(__dirname, '../', '../', 'productos.txt'), 
            { encoding: 'utf-8' }
        );
        return response ? JSON.parse(response) : [];
    }
    catch (err) {
        return err;
    }
}

export function getItemsFromFileRandom(): Item | Error {
    let allItems: Item[] | Error = getItemsFromFile();
    if(allItems instanceof Error) return allItems;
    else {
        if(allItems.length === 1) return allItems[0];
        let index: number = Math.floor(Math.random() * allItems.length);
        return allItems[index];
    }
} 