"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getItemsFromFileRandom = exports.getItemsFromFile = void 0;
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
function getItemsFromFile() {
    try {
        var response = fs_1.default.readFileSync(path_1.default.resolve(__dirname, '../', '../', 'productos.txt'), { encoding: 'utf-8' });
        return response ? JSON.parse(response) : [];
    }
    catch (err) {
        return err;
    }
}
exports.getItemsFromFile = getItemsFromFile;
function getItemsFromFileRandom() {
    var allItems = getItemsFromFile();
    if (allItems instanceof Error)
        return allItems;
    else {
        if (allItems.length === 1)
            return allItems[0];
        var index = Math.floor(Math.random() * allItems.length);
        return allItems[index];
    }
}
exports.getItemsFromFileRandom = getItemsFromFileRandom;
