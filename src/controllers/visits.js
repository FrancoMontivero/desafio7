"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getVisits = exports.addVisit = void 0;
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var PATH_FILE_VISIT = path_1.default.resolve(__dirname, '../', '../', 'visits.txt');
function addVisit(param) {
    return new Promise(function (resolve, reject) {
        var visits = getVisits();
        visits[param]++;
        fs_1.default.writeFile(PATH_FILE_VISIT, JSON.stringify(visits), function (err) {
            if (err)
                reject(err);
            else
                resolve(visits);
        });
    });
}
exports.addVisit = addVisit;
function getVisits() {
    var response = fs_1.default.readFileSync(PATH_FILE_VISIT, { encoding: 'utf-8', flag: 'a+' });
    return response ? JSON.parse(response) : { item: 0, items: 0 };
}
exports.getVisits = getVisits;
