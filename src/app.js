"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cors_1 = __importDefault(require("./middlewares/cors"));
var index_1 = __importDefault(require("./routes/index"));
var morgan_1 = __importDefault(require("./middlewares/morgan"));
var config_1 = require("./config");
var app = express_1.default();
app.use(cors_1.default);
app.use(morgan_1.default);
app.use(express_1.default.urlencoded({ extended: true }));
app.use(index_1.default);
app.set('port', config_1.PORT);
exports.default = app;
