"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var visits_1 = require("../controllers/visits");
var routerVisits = express_1.default.Router();
routerVisits.get('/visitas', function (req, res) {
    var visits = visits_1.getVisits();
    res.json({ visitas: visits });
});
exports.default = routerVisits;
