"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var routerItems = express_1.default.Router();
var visits_1 = require("../controllers/visits");
var items_1 = require("../controllers/items");
var items_2 = require("../controllers/items");
routerItems.get('/items', function (req, res) {
    visits_1.addVisit('items')
        .then(function () {
        var items = items_1.getItemsFromFile();
        if (items instanceof Error) {
            console.log(items.message);
            res.send(items.message);
        }
        else
            res.json(items);
    })
        .catch(function (err) {
        console.log(err);
    });
});
routerItems.get('/item-random', function (req, res) {
    visits_1.addVisit('item')
        .then(function () {
        var item = items_2.getItemsFromFileRandom();
        if (item instanceof Error) {
            console.log(item.message);
            res.send(item.message);
        }
        ;
        res.json(item);
    })
        .catch(function (err) {
        console.log(err);
    });
});
exports.default = routerItems;
