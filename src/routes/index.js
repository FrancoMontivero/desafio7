"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var router = express_1.default.Router();
var items_1 = __importDefault(require("./items"));
var visits_1 = __importDefault(require("./visits"));
router.use(items_1.default);
router.use(visits_1.default);
exports.default = router;
